package steinar;

import steinar.test.LuhnTester;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        LuhnTester.runTests();

        Scanner sc = new Scanner(System.in);
        String line = "default";
        System.out.println("Enter a string of numbers to check whether it's Luhn valid.");

        while (sc.hasNextLine()){
            line = sc.nextLine();
            if (line.trim().equals("")){ break; }
            Luhn.numIsLuhnValid(line, true);
        }
    }
}
