package steinar;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class Luhn {
    //https://en.wikipedia.org/wiki/Luhn_algorithm
    private final static int N_CREDIT_CARD = 16;


    private static boolean isInt(char c){
        try {
            Integer.parseInt(c+"");
            return true;
        } catch (NumberFormatException ex){
            return false;
        }
    }
    private static boolean isInt(String s){
        String regex = "\\d+"; //Digits (1 or more)
        return s.matches(regex);
    }

    /**
     *
     * @param numWithoutDigit is a Luhn id WITHOUT the last digit
     * @return
     */
    public static int getCheckSumDigit(String numWithoutDigit){
        // Make array of digits string, zeroing all non-nums.
        int[] digits = new int[numWithoutDigit.length()];
        final int rightmost = digits.length-1;

        for (int i=0; i<digits.length; i++){
            digits[i] = Integer.parseInt(""+numWithoutDigit.charAt(i));
        }

        // 1) From the rightmost digit (excluding check), and moving left:
        //  - double the value of every second digit.
        //  - If the result of this doubling is >9: Add digits to sum.
        //  - Alternatively: Subtract 9 from doubled over 9. (same)

        int sum = 0;
        for (int left = 0; left <= rightmost; left++){
            final int at = rightmost - left;

            if (left % 2 == 0){ //Every other, skipping first 'x' (excluded) in input arg
                digits[at] *= 2; //double
                if (digits[at] > 9){
                    digits[at] -= 9;
                }
            }
            sum += digits[at];
        }

        //System.out.println(Arrays.toString(digits));
        //The check digit (x) is obtained by computing the sum of the
        //sum digits, then computing 9 times that value modulo 10
        int checkDigit = (sum*9)%10;
        return checkDigit;
    }

    /**
     * Adds a check digit using Luhn's Algorithm.
     * @param numToAddDigitOnto A number of n digits
     * @return A checked number with n+1 digits
     */
    public static String addCheckSumDigit(String numToAddDigitOnto){
        int digit = getCheckSumDigit(numToAddDigitOnto);
        return numToAddDigitOnto + digit;
    }


    private static void sopl(boolean doPrint, String text){
        if (!doPrint) return;
        System.out.println(text);
    }

    /**
     *
     * @param numWithCheck Luhn compatible number string
     * @return Whether the checksum is correct (0)
     */
    public static boolean numIsLuhnValid(String numWithCheck){
        return numIsLuhnValid(numWithCheck, false);
    }
    public static boolean numIsLuhnValid(String numWithCheck, boolean doPrint){
        if (isInt(numWithCheck) == false){
            sopl(doPrint, "❌ Invalid. \""+numWithCheck+"\" is not a number!");
            return false;
        }

        int checkPosition = numWithCheck.length() - 1; //last
        String content = numWithCheck.substring(0, checkPosition);
        char checkDigit = numWithCheck.charAt(checkPosition);
        int expectedDigit = getCheckSumDigit(content);
        int checkDigitValue = Integer.parseInt(""+checkDigit);
        boolean isValid = checkDigitValue == expectedDigit;

        if (doPrint){
            System.out.println("Input number: "+content+", checksum digit: "+checkDigit);
            if (isValid){
                System.out.println("✅ Valid! Matched expected: "+expectedDigit);
                if (numWithCheck.length() == N_CREDIT_CARD){
                    System.out.println("Digits: "+N_CREDIT_CARD+" (valid credit card)");
                }
            } else {
                System.out.println("❌ Invalid. Expected: "+expectedDigit);
            }
        }

        return isValid;
    }
}
