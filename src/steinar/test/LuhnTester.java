package steinar.test;

import org.junit.Assert;
import org.junit.Test;
import steinar.Luhn;

public class LuhnTester {

    @Test
    public static void runTests(){
        //https://en.wikipedia.org/wiki/Luhn_algorithm
        //String wiki = "7992739871";
        //System.out.println(Luhn.getCheckSumDigit(wiki));

        Assert.assertEquals(Luhn.getCheckSumDigit("424242424242424"),2);
        Assert.assertTrue(Luhn.numIsLuhnValid("4242424242424242"));

        Assert.assertEquals(Luhn.getCheckSumDigit("7992739871"), 3);
        Assert.assertTrue(Luhn.numIsLuhnValid("79927398713"));

        Assert.assertFalse(Luhn.numIsLuhnValid(""));
        Assert.assertFalse(Luhn.numIsLuhnValid("abcdef"));
        Assert.assertFalse(Luhn.numIsLuhnValid("1234"));

        Assert.assertTrue(Luhn.numIsLuhnValid("0000"));
        Assert.assertTrue(Luhn.numIsLuhnValid("0018"));
        Assert.assertTrue(Luhn.numIsLuhnValid("0109"));
        Assert.assertTrue(Luhn.numIsLuhnValid("0117"));
        Assert.assertTrue(Luhn.numIsLuhnValid("1008"));
        Assert.assertTrue(Luhn.numIsLuhnValid("1230"));
    }
}
